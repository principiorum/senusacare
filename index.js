/** @format */

import {AppRegistry, YellowBox} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';

YellowBox.ignoreWarnings(['ListView is deprecated and will be removed in a future release. See https://fb.me/nolistview for more information']);
AppRegistry.registerComponent(appName, () => App);
