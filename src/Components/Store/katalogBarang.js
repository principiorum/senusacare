import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, FlatList, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class KatalogBarang extends Component {
	state = {
		diet: [
			{
				name: 'Set Masker',
				price: 'Rp 120.000',
				img: require('../../Assets/mask1.jpg'),
			},
			{
				name: 'Set Masker',
				price: 'Rp 80.000',
				img: require('../../Assets/mask2.jpg'),
			},
			{
				name: 'Set Masker',
				price: 'Rp 60.000',
				img: require('../../Assets/mask3.jpg'),
			},
			{
				name: 'Set Masker',
				price: 'Rp 40.000',
				img: require('../../Assets/mask2.jpg'),
			},
			{
				name: 'Set Masker',
				price: 'Rp 68.000',
				img: require('../../Assets/mask1.jpg'),
			},
			{
				name: 'Set Masker',
				price: 'Rp 60.000',
				img: require('../../Assets/mask3.jpg'),
			},
		]
	}

	render() {
		const {
			header,
			textt,
			textt2,
			card,
			inputContainer,
			images
		} = styles;

		return (
			<View style={styles.container}>

				<View style={header}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
              <Icon 
                name="angle-left"
                size={30}
                color="#ffffff"
              />
            </TouchableOpacity>
						<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Store</Text>
					</View>

					<View 
						style={inputContainer}
					>
						<Icon 
              name="search"
              size={20}
              style={{ marginRight: 25 }}
              color="#ffffff"
            />
						<TextInput
							placeholder='Cari Berdasarkan nama Product'
							placeholderTextColor='#ffffff'
							style={{ color: '#ffffff' }}
							underlineColorAndroid="transparent"
						/>
					</View>
				</View>
				
				<View style={{ paddingHorizontal: 20, flex: 1 }}>
					<FlatList
						data={this.state.diet}
						numColumns={2}
						showsVerticalScrollIndicator={false}
						columnWrapperStyle={{ justifyContent: 'space-between', paddingTop: 20, paddingBottom: 20 }}
						keyExtractor={(item, index) => index.toString()}
						renderItem={({ item }) =>
							<TouchableOpacity style={card} onPress={() => Actions.keranjangStore({ names: item.name, price: item.price, img: item.img })}>
								<Image
									style={images}
									source={item.img}
								/>
								<View style={{ padding: 5, justifyContent: 'space-between', flex: 1 }}>
									<Text style={textt}>{item.name}</Text>
									<Text style={textt2}>{item.price}</Text>
								</View>
							</TouchableOpacity>
						}
					/>
				</View>
			
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 10,
		color: '#231f20',
	},
	textt2: {
		fontSize: 14,
		color: '#231f20',
		fontWeight: '900',
	},
	header: {
		height: 180,
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE',
		justifyContent: 'space-between',
		paddingTop: 40,
		paddingBottom: 30
	},
	card: {
		width: 150,
		height: 200,
		borderRadius: 5,
		elevation: 5,
		backgroundColor: '#ffffff',
	},
	images: { 
		width: '100%', 
		height: 145, 
		borderTopLeftRadius: 5, 
		borderTopRightRadius: 5  
	},
	inputContainer: { 
		alignItems: 'center', 
		height: 50, 
		flexDirection: 'row', 
		width: '100%', 
		backgroundColor: '#ABD2FC', 
		borderRadius: 5, 
		paddingHorizontal: 15 
	}
};
