import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class JenisDiet extends Component {
	state = {
		diet: [
			{
				name: 'Menu Diet 1',
				price: '120.000',
				img: require('../../Assets/sayur.jpg'),
			},
			{
				name: 'Menu Diet 2',
				price: '80.000',
				img: require('../../Assets/sayur2.jpg'),
			},
			{
				name: 'Menu Diet 3',
				price: '60.000',
				img: require('../../Assets/sayur3.jpg'),
			},
			{
				name: 'Menu Diet 4',
				price: '40.000',
				img: require('../../Assets/sayur4.jpg'),
			},
			{
				name: 'Menu Diet 5',
				price: '68.000',
				img: require('../../Assets/sayur5.jpg'),
			},
			{
				name: 'Menu Diet 6',
				price: '60.000',
				img: require('../../Assets/sayur6.jpg'),
			},
		]
	}

	render() {
		const {
			header,
			textt,
			textt2,
			card,
			images
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>{this.props.judul}</Text>
				</View>
				
				<View style={{ paddingHorizontal: 20, flex: 1 }}>
					<FlatList
						data={this.state.diet}
						numColumns={2}
						showsVerticalScrollIndicator={false}
						columnWrapperStyle={{ justifyContent: 'space-between', paddingTop: 20, paddingBottom: 20 }}
						keyExtractor={(item, index) => index.toString()}
						renderItem={({ item }) =>
							<TouchableOpacity style={card} onPress={() => Actions.menuDiet({judul: item.name, price: item.price, img: item.img})}>
								<Image
									style={images}
									source={item.img}
								/>
								<View style={{ padding: 5, justifyContent: 'space-between', flex: 1 }}>
									<Text style={textt}>{item.name}</Text>
									<Text style={textt2}>Rp {item.price}</Text>
								</View>
							</TouchableOpacity>
						}
					/>
				</View>
			
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 10,
		color: '#231f20',
	},
	textt2: {
		fontSize: 14,
		color: '#231f20',
		fontWeight: '900',
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	card: {
		width: 150,
		height: 200,
		borderRadius: 5,
		elevation: 5,
		backgroundColor: '#ffffff',
	},
	images: { 
		width: '100%', 
		height: 145, 
		borderTopLeftRadius: 5, 
		borderTopRightRadius: 5  
	}
};
