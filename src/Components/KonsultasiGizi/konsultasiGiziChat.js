import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, FlatList, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class KonsultasiGiziChat extends Component {
	state = {
		tmpChat: null,
		chat: [
			{ text: 'Hai, silahkan tanyakan hal yang anda ingin ketahui', time: '', admin: true },
		]
	}

	appendChatUser() {
		let dates = new Date().toLocaleTimeString();
		if (this.state.tmpChat) {
			this.setState({
				chat: [{ text: this.state.tmpChat, time: dates, admin: false }, ...this.state.chat]
			});
		}
		this.setState({ tmpChat: null });
	}

	render() {
		const {
			header,
			orderButton,
			bottomContainer,
			promoCode,
			chatUser,
			chatAdmin
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Konsultasi Gizi</Text>
				</View>
					<View style={{ flex: 1, paddingHorizontal: 10 }}>
					<FlatList
						data={this.state.chat}
						numColumns={1}
						showsVerticalScrollIndicator={false}
						inverted
						keyExtractor={(item, index) => index.toString()}
						renderItem={({ item }) =>
							<View style={[chatUser, item.admin && chatAdmin]}>
								<Text style={{ color: '#ffffff', fontSize: 12 }}>
									{item.text}
								</Text>
								<Text style={{ alignSelf: 'flex-end', fontSize: 10, color: '#ffffff' }}>
									{item.time}
								</Text>
							</View>
						}
					/>
					</View>

				<View style={bottomContainer}>
					<TextInput
						style={promoCode}
						placeholder='Mulai chat'
						multiline
						value={this.state.tmpChat}
						onChangeText={value => this.setState({ tmpChat: value })}
						spellCheck={false}
						autoCorrect={false}
					/>

					<TouchableOpacity style={orderButton} onPress={() => this.appendChatUser()}>
						<Image
							style={{ width: 25, height: 25 }}
							source={require('../../Assets/icon-another/next.png')}
						/>
					</TouchableOpacity>

				</View>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	chatUser: { 
		backgroundColor: '#A9A9A9', 
		width: 250, 
		padding: 10, 
		alignSelf: 'flex-end',
		borderRadius: 5,
		elevation: 5,
		marginBottom: 10
	},
	chatAdmin: { 
		backgroundColor: '#7AB4FE', 
		width: 250, 
		padding: 10, 
		borderRadius: 5,
		alignSelf: 'flex-start',
		elevation: 5,
		marginBottom: 10
	},
	bottomContainer: {
		minHeight: 75,
		maxHeight: 200,
		width: '100%',
		paddingHorizontal: 20,
		borderTopWidth: 1,
		borderColor: '#e9e9e9',
		alignItems: 'flex-end',
		paddingVertical: 10,
		flexDirection: 'row',
		backgroundColor: '#ffffff',
		justifyContent: 'space-between',
	},
	orderButton: { 
		alignItems: 'center', 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		height: 50,
		width: 50,
		borderRadius: 25,
	},
	promoCode: {
		flex: 1,
		fontSize: 12,
		minHeight: 40, 
		maxHeight: 200,
		backgroundColor: '#e9e9e9',
		color: '#231f20',
		borderRadius: 5,
		paddingHorizontal: 10,
		marginRight: 15,
	},

};
