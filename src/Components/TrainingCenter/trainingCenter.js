import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const width = Dimensions.get('window').width;

export default class TrainingCenterHome extends Component {

	render() {
		const {
			header,
			textt,
			textt2,
			button
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Training Center</Text>
				</View>
				<ScrollView>
					<View style={{ justifyContent: 'space-between', paddingTop: 20, flex: 1 }}>
							<TouchableOpacity style={button} onPress={() => Actions.trainingCenterDetail()}>
								<Image
									style={{ width: '100%', height: 170, borderRadius: 10 }}
									source={require('../../Assets/treatment1.jpg')}
								/>
								
								<Text style={textt}>PELATIHAN GOLDEN TREATMENT FOR MOM AND BABY</Text>
								<Text style={textt2}>SENIN, 25 NOV 2018, 09:00</Text>
								
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.trainingCenterDetail()}>
								<Image
									style={{ width: '100%', height: 170, borderRadius: 5 }}
									source={require('../../Assets/treatment2.jpg')}
								/>
								
									<Text style={textt}>WORKSHOP DAN SOSIALISASI</Text>
									<Text style={textt2}>SENIN, 25 NOV 2018, 09:00</Text>
								
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.trainingCenterDetail()}>
								<Image
									style={{ width: '100%', height: 170, borderRadius: 5 }}
									source={require('../../Assets/treatment1.jpg')}
								/>
								
									<Text style={textt}>PIKNIK DI PANTAI KUTA</Text>
									<Text style={textt2}>SENIN, 25 NOV 2018, 09:00</Text>
								
							</TouchableOpacity>
					</View>
				</ScrollView>
			
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 16,
		color: '#231f20',
		fontWeight: '900',
		marginTop: 5,
		marginBottom: 5,
	},
	textt2: {
		fontSize: 12,
		color: '#231f20',
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	button: { 
		marginBottom: 20, 
		padding: 10,
		flex: 1,
		backgroundColor: '#ffffff',
		elevation: 5,
		alignSelf: 'center',
		width: width - 40,
		justifyContent: 'space-between',
		borderRadius: 8
	}
};

