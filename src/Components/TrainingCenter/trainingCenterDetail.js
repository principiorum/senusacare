import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
//import MapView from 'react-native-maps';

const width = Dimensions.get('window').width;

export default class TrainingCenterDetail extends Component {

	render() {
		const {
			header,
			textt,
			textt2,
			textt3,
			card,
			iconStyle,
			orderButton
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
						<Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
						/>
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Training Center</Text>
				</View>
				<ScrollView>
					<Image
						style={{ width: '100%', height: 200, position: 'absolute', top: 0 }}
						source={require('../../Assets/img1.jpg')}
					/>

					<View style={card}>
						<Text style={textt2}>PELATIHAN GOLDER TREATMENT FOR MOM AND BABY</Text>

						<View style={{ flexDirection: 'row', marginBottom: 15 }}>
							<Image
								style={iconStyle}
								source={require('../../Assets/icon-another/calendar.png')}
							/>
							<Text style={textt}>Sabtu, 20 Desember 2018{'\n'}09:00 WIB</Text>
						</View>

						<View style={{ flexDirection: 'row', marginBottom: 15 }}>
							<Image
								style={iconStyle}
								source={require('../../Assets/icon-another/pin.png')}
							/>
							<Text style={textt}>Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan</Text>
						</View>

						<View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center' }}>
							<Image
								style={iconStyle}
								source={require('../../Assets/icon-another/pricetag.png')}
							/>
							<Text style={textt}>Rp 45.000</Text>
						</View>
					</View>

					<View style={{ ...card, marginTop: 20, marginBottom: 20 }}>
						<Text style={textt2}>Deskripsi acara</Text>
						<Text style={textt}>Juga dibuka JOB Fair untuk penerimaan  bidan dan perawat siap bekerja</Text>
					</View>

					<Text style={textt3}>Lokasi</Text>
					{/*
						<MapView
							initialRegion={{
								latitude: 37.78825,
								longitude: -122.4324,
								latitudeDelta: 0.0922,
								longitudeDelta: 0.0421,
							}}
							style={{
								width: '100%',
								height: 300
							}}
						/>
					*/}

					<Image
						style={{ width: '100%', height: 200, marginBottom: 15 }}
						source={require('../../Assets/icon-another/maps.png')}
					/>

					<TouchableOpacity style={orderButton} onPress={() => Actions.trainingCenterOrder()}>
						<Text style={{ fontSize: 14, color: '#ffffff', fontWeight: '500' }}>JOIN</Text>
					</TouchableOpacity>
				</ScrollView>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: { 
		fontSize: 12, 
		lineHeight: 12, 
		flex: 1, 
		color: '#231f20' 
	},
	textt2: { 
		fontSize: 14, 
		fontWeight: '900', 
		color: '#231f20',
		marginBottom: 15 
	},
	textt3: { 
		fontSize: 14, 
		fontWeight: '900', 
		color: '#231f20', 
		marginBottom: 10, 
		marginLeft: 20 
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	card: { 
		padding: 15,
		marginTop: 180, 
		width: width - 40,
		alignSelf: 'center', 
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5,
		justifyContent: 'space-between'
	},
	iconStyle: { 
		height: 20, 
		width: 20, 
		marginRight: 10 
	},
	orderButton: { 
		alignItems: 'center',
		width: width - 40, 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		paddingHorizontal: 30,
		paddingVertical: 10,
		borderRadius: 5,
		alignSelf: 'center',
		marginBottom: 15
	},
};
