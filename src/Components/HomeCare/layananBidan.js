import React, { Component } from 'react';
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const width = Dimensions.get('window').width;

export default class LayananBidan extends Component {
	state = {
		features: [
			{
				name: 'Sekolah ibu Hamil',
				price: 'Rp 300.000',
				img: require('../../Assets/icon-homecare/maternity.png'),
			},
			{
				name: 'Exercise (Yoga, Senam & Tarian Persalinan',
				price: 'Rp 300.000',
				img: require('../../Assets/icon-homecare/meditation.png'),
			},
			{
				name: 'Kelas Persiapan Persalinan',
				price: 'Rp 300.000',
				img: require('../../Assets/icon-store/mother.png'),
			},
			{
				name: 'Reposisi letak sungsang, Lintang & Lilitan Tali Pusat',
				price: 'Rp 300.000',
				img: require('../../Assets/icon-homecare/pregnant.png'),
			},
			{
				name: 'Pendampingan Persalinan',
				price: 'Rp 300.000',
				img: require('../../Assets/icon-homecare/maternity-1.png'),
			},
			{
				name: 'Konselor Asi',
				price: 'Rp 300.000',
				img: require('../../Assets/icon-homecare/breastfeeding.png'),
			},
			{
				name: 'Treatment Baby Massage & Baby Spa',
				price: 'Rp 300.000',
				img: require('../../Assets/icon-homecare/baby.png'),
			},
			{
				name: 'Perawatan Pendamping',
				price: 'Rp 300.000',
				img: require('../../Assets/icon-homecare/support.png'),
			},
		]
	}

	render() {
		const {
			header,
			overlay,
			textt,
			textt2,
			card
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Layanan Kebidanan</Text>
				</View>
				
				<View style={{ flex: 1 }}>
					<FlatList
						data={this.state.features}
						numColumns={1}
						showsVerticalScrollIndicator={false}
						keyExtractor={(item, index) => index.toString()}
						renderItem={({ item }) =>
							<TouchableOpacity style={card} onPress={() => Actions.pesanHomeCare({ judul: item.name })}>
								<View style={overlay}>
									<Text style={textt}>{item.name}</Text>
									<Text style={textt2}>{item.price}</Text>
								</View>

								<Image
									style={{ width: 40, height: 40, alignSelf: 'center', marginRight: 20 }}
									source={item.img}
								/>
							</TouchableOpacity>
						}
					/>
				</View>
			
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 16,
		fontWeight: '900',
		color: '#231f20'
	},
	textt2: {
		fontSize: 16,
		color: '#231f20'
	},
	overlay: { 
		justifyContent: 'space-between',
		flex: 1
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	card: { 
		padding: 15,
		marginBottom: 10,
		marginTop: 10, 
		height: 100, 
		width: width - 40,
		alignSelf: 'center', 
		elevation: 5, 
		flexDirection: 'row',
		justifyContent: 'space-between',
		backgroundColor: '#ffffff',
		borderRadius: 5
	}
};
