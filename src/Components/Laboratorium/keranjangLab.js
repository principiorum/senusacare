import React, { Component } from 'react';
import { View, Text, Dimensions, Image, TouchableOpacity, ScrollView, TouchableWithoutFeedback, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const width = Dimensions.get('window').width;

export default class KeranjangLab extends Component {
	state = {
		cash: true,
		transfer: false,
	}

	render() {
		const {
			header,
			header2,
			textt,
			texttt,
			textt2,
			card,
			totalTextt,
			orderButton,
			dotInactive,
			promoCode,
			dotActive
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
						<Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
						/>
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Keranjang</Text>
				</View>
				<View style={header2} />
				<ScrollView>
				<View style={{ flex: 1 }}>
					{
						// Item detail
					}
					<View style={{ ...card, flexDirection: 'row' }}>
						<Image
							style={{ width: 80, height: 80, borderRadius: 5, marginRight: 10 }}
							source={require('../../Assets/blood.jpg')}
						/>
						<View style={{ justifyContent: 'space-between' }}>
							<Text style={textt2}>{this.props.judul}</Text>
							<Text style={{ fontSize: 20, color: '#231f20', fontWeight: '600' }}>Rp 120.000</Text>
						</View>
					</View>
					{
						//Top card Lokasi dan waktu pemesanan
					}
					<View style={card}>
						<Text style={textt2}>Lokasi Pengiriman</Text>
					
						<View style={{ flexDirection: 'row', marginBottom: 10 }}>
							<Image
								style={{ height: 20, width: 20, marginRight: 10 }}
								source={require('../../Assets/icon-another/pin.png')}
							/>
							<Text style={{ fontSize: 10, flex: 1, lineHeight: 10 }}>Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan</Text>
						</View>

						<TextInput
							style={promoCode}
							placeholder='Detail Lokasi'
							multiline
							underlineColorAndroid='transparent'
							spellCheck={false}
							autoCorrect={false}
						/>
					</View>
					{
						//Second card, Detail Pembayaran
					}
					<View style={card}>
						<Text style={textt2}>Detail Pembayaran</Text>
					
						<TouchableWithoutFeedback onPress={() => this.setState({ cash: true, transfer: false })}>
							<View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center' }}>
								<View style={[dotInactive, this.state.cash && dotActive]} />
								<Text style={textt}>Bayar Tunai Ditempat</Text>
							</View>
						</TouchableWithoutFeedback>


						<TouchableWithoutFeedback onPress={() => this.setState({ cash: false, transfer: true })}>
							<View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center' }}>
								<View style={[dotInactive, this.state.transfer && dotActive]} />
								<Text style={textt}>Bank Transfer</Text>
							</View>
						</TouchableWithoutFeedback>

						<View style={{ flexDirection: 'row', marginLeft: 24, marginBottom: 15 }}>
							<Image
								style={{ height: 40, width: 80, marginRight: 10 }}
								source={require('../../Assets/icon-another/logo-bni.png')}
							/>

							<View style={{ flex: 1 }}>
								<Text style={{ fontSize: 12, color: '#231f20', lineHeight: 12 }}>Transfer Ke:</Text> 
								<Text style={{ fontSize: 12, color: '#7AB4FE' }}>0741325741</Text> 
								<Text style={{ fontSize: 10, color: '#231f20' }}>A/n yayasan sosial teknologi nusantara</Text> 
							</View>
						</View>

						<View style={totalTextt}>
							<Text style={texttt}>Total Pembayaran</Text>
							<Text style={{ ...texttt, fontWeight: '500' }}>Rp 300.022</Text>
						</View>
					</View>

				</View>

				<TouchableOpacity style={orderButton}>
					<Text style={{ fontSize: 14, color: '#ffffff', fontWeight: '500' }}>BAYAR</Text>
				</TouchableOpacity>
				</ScrollView>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: { 
		fontSize: 12, 
		color: '#231f20', 
		flex: 1 
	},
	texttt: { 
		fontSize: 12, 
		color: '#231f20', 
	},
	textt2: { 
		fontSize: 14, 
		fontWeight: '900', 
		color: '#231f20',
		marginBottom: 15 
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	header2: {
		height: 60,
		backgroundColor: '#7AB4FE',
		position: 'absolute',
		top: 80,
		left: 0,
		right: 0,
	},
	card: { 
		padding: 15,
		width: width - 40,
		alignSelf: 'center',
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5,
		marginBottom: 15,
	},
	totalTextt: { 
		flexDirection: 'row', 
		marginBottom: 15, 
		justifyContent: 'space-between',
		marginLeft: 24
	},
	orderButton: { 
		alignItems: 'center',
		width: width - 40, 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		paddingHorizontal: 30,
		paddingVertical: 10,
		borderRadius: 5,
		alignSelf: 'center',
		marginBottom: 15
	},
	dotInactive: { 
		height: 14, 
		width: 14, 
		borderRadius: 7,
		borderWidth: 1,
		borderColor: '#F88C8F',
		marginRight: 10
	},
	dotActive: { 
		height: 14, 
		width: 14, 
		borderRadius: 7,
		borderWidth: 1,
		borderColor: '#F88C8F',
		marginRight: 10,
		backgroundColor: '#F88C8F'
	},
	promoCode: {
		flex: 1,
		height: 80,
		backgroundColor: '#e9e9e9',
		color: '#231f20',
		borderRadius: 5,
		paddingHorizontal: 10,
		marginLeft: 30,
		fontSize: 12,
		textAlignVertical: 'top',
	}
};
