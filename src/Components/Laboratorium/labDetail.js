import React, { Component } from 'react';
import { View, Text, Dimensions, Image, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import DatePicker from 'react-native-datepicker';

const width = Dimensions.get('window').width;

export default class LabDetail extends Component {
  state = {
    tanggal: null
  }

  render() {
    const {
      header,
      textt,
      card,
      card2,
      orderButton,
    } = styles;

    return (
      <View style={styles.container}>
        <View style={header}>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
              <Icon 
              name="angle-left"
              size={30}
              color="#ffffff"
              />
            </TouchableOpacity>
            <Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>{this.props.judul}</Text>
          </View>
          <Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff', marginLeft: 40 }}>Rp.150.000</Text>
        </View>
        <ScrollView>
          <Image
            style={{ width: '100%', height: 180, position: 'absolute', top: 0 }}
            source={require('../../Assets/blood.jpg')}
          />

          <View style={{ flex: 1 }}>
            <View style={card}>
              <Text style={{ fontSize: 14, fontWeight: '900', color: '#231f20', marginBottom: 15 }}>Parameter Test</Text>
              <Text style={textt}>1. Test 1</Text>
              <Text style={textt}>2. Test 2</Text>
              <Text style={textt}>3. Test 3</Text>
              <Text style={textt}>4. Test 4</Text>
              <Text style={textt}>5. Test 5</Text>
              <Text style={textt}>6. Test 6</Text>
              <Text style={textt}>7. Test 7</Text>
              <Text style={{ fontSize: 10, fontWeight: '600' }}>8. Test 8</Text>
            </View>
            <View style={card2}>
              <Text style={{ fontSize: 12, color: '#231f20', fontWeight: '600' }}>Tanggal</Text>

              <View style={{ backgroundColor: '#e9e9e9', marginTop: 10, padding: 5, borderRadius: 5 }}>
              <DatePicker
                  style={{ width: '100%', }}
                  date={this.state.tanggal}
                  mode="date"
                  placeholder='Pilih Tanggal'
                  format="DD MMMM YYYY"
                  minDate="10 January 1901"
                  maxDate="10 January 2029"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  showIcon={false}
                  customStyles={{
                    dateInput: {
                      borderWidth: 0,
                      width: '100%',
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                      paddingVertical: 0,
                      marginLeft: 10
                    },
                    placeholderText: {
                      fontSize: 12,
                      color: '#231f20',
                      margin: 0
                    },
                    dateTouchBody: {
                      paddingVertical: 12,
                    }
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(date1) => { 
                    this.setState({ tanggal: date1 }); 
                  }}
                />
                </View>
            </View>

            <TouchableOpacity style={orderButton} onPress={() => Actions.keranjangLab({ judul: this.props.judul })}>
              <Text style={{ fontSize: 14, color: '#ffffff', fontWeight: '500' }}>BELI</Text>
            </TouchableOpacity>
        </View>
        </ScrollView>
        
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
  },
  textt: { 
    fontSize: 10, 
    marginBottom: 15, 
    fontWeight: '600' 
  },
  header: { 
    height: 80, 
    backgroundColor: '#7AB4FE', 
    justifyContent: 'center', 
    paddingHorizontal: 20 
  },
  card: { 
    padding: 15,
    width: width - 40,
    alignSelf: 'center',
    elevation: 5, 
    backgroundColor: '#ffffff',
    borderRadius: 5,
    marginBottom: 15,
    marginTop: 120
  },
  card2: { 
    padding: 15,
    width: width - 40,
    alignSelf: 'center',
    elevation: 5, 
    backgroundColor: '#ffffff',
    borderRadius: 5,
    marginBottom: 15 
  },
  orderButton: { 
    alignItems: 'center',
    width: width - 40, 
    justifyContent: 'center', 
    backgroundColor: '#F88C8F', 
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 5,
    alignSelf: 'center',
    marginBottom: 15
  },
};
