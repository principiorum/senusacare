import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const width = Dimensions.get('window').width;

export default class InfoDetail extends Component {

	render() {
		const {
			header,
			header2,
			textt,
			textt2,
			card,
			card2,
			orderButton,
			bottomContainer,
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>{this.props.judul}</Text>
				</View>
				<View style={header2} />
				<View style={{ flex: 1 }}>
					<View style={card}>
						<Text style={{ fontSize: 14, fontWeight: '900', color: '#231f20', marginBottom: 15 }}>Meliputi</Text>
						<Text style={{ fontSize: 10, color: '#231f20', marginBottom: 15 }}>1. Sekolah Ibu Hamil</Text>
						<Text style={{ fontSize: 10, color: '#231f20', marginBottom: 15 }}>2. Periksa Kehamilan</Text>
						<Text style={{ fontSize: 10, color: '#231f20', marginBottom: 15 }}>3. Persiapan Persalinan</Text>
						<Text style={{ fontSize: 10, color: '#231f20' }}>4. Rileksasi</Text>
					</View>
					<View style={card2}>
						<Text style={{ fontSize: 10, color: '#231f20' }}>Durasi</Text>
						<Text style={{ fontSize: 15, color: '#7AB4FE', fontWeight: '600' }}>120 Menit/
							<Text style={{ fontSize: 15, color: '#231f20', fontWeight: '600' }}>Sesi (3x Pertemuan)</Text>
						</Text>
					</View>
				</View>

				<View style={bottomContainer}>
					<View>
						<Text style={{ fontSize: 14, color: '#231f20', fontWeight: '500' }}>Total Harga</Text>
						<Text style={{ fontSize: 16, color: '#7AB4FE', fontWeight: '500' }}>Rp 600.000</Text>
					</View>

					<TouchableOpacity style={orderButton} onPress={() => Actions.konfirmasiPesananBidan()}>
						<Text style={{ fontSize: 14, color: '#ffffff', fontWeight: '500' }}>BAYAR</Text>
					</TouchableOpacity>

				</View>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 16,
		fontWeight: '900',
		color: '#231f20'
	},
	textt2: {
		fontSize: 16,
		color: '#231f20'
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	header2: {
		height: 80,
		backgroundColor: '#7AB4FE',
		position: 'absolute',
		top: 80,
		left: 0,
		right: 0,
	},
	card: { 
		padding: 15,
		width: width - 40,
		alignSelf: 'center',
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5,
		marginBottom: 15
	},
	card2: { 
		padding: 15,
		width: width - 40,
		alignSelf: 'center',
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5,
	},
	bottomContainer: {
		height: 80,
		width: '100%',
		paddingHorizontal: 20,
		borderTopWidth: 1,
		borderColor: '#e9e9e9',
		alignItems: 'center',
		flexDirection: 'row',
		backgroundColor: '#ffffff',
		justifyContent: 'space-between',
	},
	orderButton: { 
		alignItems: 'center', 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		paddingHorizontal: 30,
		paddingVertical: 10,
		borderRadius: 5 
	},
};
