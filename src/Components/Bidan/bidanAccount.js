import React, { Component } from 'react';
import { View, Text, FlatList, Image, ImageBackground, ScrollView, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class BidanAccount extends Component {
  state = {
    services: [
      {
        nama: 'Paket Hamil',
        img: require('../../Assets/sayur.jpg'),
      },
      {
        nama: 'Paket Hamil 2',
        img: require('../../Assets/sayur2.jpg')
      },
      {
        nama: 'Paket Hamil 3',
        img: require('../../Assets/sayur3.jpg')
      },
      {
        nama: 'Paket Hamil 4',
        img: require('../../Assets/sayur4.jpg')
      },
      {
        nama: 'Paket Hamil 5',
        img: require('../../Assets/sayur5.jpg')
      },
    ]
  }

  render() {
    const {
      header,
      topContainer,
      imageContainer,
      openSign,
      textt,
      overlay,
    } = styles;

    return (
      <View style={styles.container}>
       <ScrollView>
            <Image
              style={{ width: '100%', height: 250 }}
              source={require('../../Assets/img1.jpg')}
            />

            <TouchableOpacity style={{ position: 'absolute', left: 20, top: 25 }} onPress={() => Actions.pop()}>
              <Icon 
              name="angle-left"
              size={30}
              color="#ffffff"
              />
            </TouchableOpacity>

            <View style={topContainer}>
              {
                // photos and open sign
              }
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10 }}>
                <View style={imageContainer}>
                  <Image 
                    style={{ width: 120, height: 120, borderRadius: 60 }} 
                    source={this.props.img}
                  />
                </View>
                {
                  // open sign
                }
                <View style={{ alignSelf: 'flex-end', alignItems: 'center', flexDirection: 'row', paddingBottom: 15 }}>
                  <View style={openSign}>
                    <Text style={{ fontSize: 12, fontWeight: '600', color: '#ffffff' }}>BUKA</Text>
                  </View>
                  <Text style={{ fontSize: 12, fontWeight: '600', color: '#231f20' }}>09:00 - 16:00 WIB</Text>
                </View> 
              </View>
            

              {
                // Info
              }
              <View style={{ marginBottom: 20 }}>
                <Text style={{ fontSize: 14, fontWeight: '800', color: '#231f20' }}>{this.props.nama}</Text>
                <Text style={{ fontSize: 12, textAlign: 'justify' }}>{this.props.alamat}</Text>
              </View>

              {
                // Services
              }
              <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 12, marginBottom: 10 }}>PILIH PAKET</Text>
                <FlatList
                  data={this.state.services}
                  numColumns={2}
                  showsVerticalScrollIndicator={false}
                  columnWrapperStyle={{ justifyContent: 'space-between', paddingBottom: 20 }}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item }) =>
                    <TouchableOpacity onPress={() =>  Actions.orderServiceBidan({ judul: item.nama })}>
                      <ImageBackground
                        style={{ width: 150, height: 80, borderRadius: 5 }}
                        imageStyle={{ borderRadius: 5 }}
                        source={item.img}
                      >
                        <View style={overlay}>
                          <Text style={textt}>{item.nama}</Text>
                        </View>
                      </ImageBackground>
                    </TouchableOpacity>
                  }
                />
              </View>
            </View>
           
          
       </ScrollView>
       </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
  },
  topContainer: { 
    paddingHorizontal: 20,
    position: 'relative',
    bottom: 60,
  },
  imageContainer: { 
    width: 124, 
    height: 124, 
    borderRadius: 62, 
    backgroundColor: '#ffffff', 
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  header: {
    height: 240,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#7AB4FE'
  },
  openSign: { 
    backgroundColor: '#7AB4FE', 
    borderRadius: 5, 
    paddingHorizontal: 15, 
    paddingVertical: 5, 
    marginRight: 10 
  },
  textt: {
    fontSize: 12,
    color: '#ffffff',
    fontWeight: '900',
    textAlign: 'center'
  },
  overlay: { 
    height: 80,
    width: 150,
    backgroundColor: 'rgba(0,0,0,0.4)', 
    borderRadius: 5, 
    justifyContent: 'center', 
    alignItems: 'center',
  },
};
