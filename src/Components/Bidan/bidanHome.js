/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import  { Actions } from 'react-native-router-flux';

export default class BidanHome extends Component {

	state = {
		bidan: [
			{
				nama: 'Tri Wahyuni',
				alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan',
				img: require('../../Assets/doc1.jpg'),
				link: function go(nama, alamat, img) { Actions.bidanAccount({ nama, alamat, img }); }
			},
			{
				nama: 'Sri Yuliani A',
				alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan',
				img: require('../../Assets/doc2.jpg'),
				link: function go(nama, alamat, img) { Actions.bidanAccount({ nama, alamat, img }); }
			},
			{
				nama: 'Aprilia Yusrina',
				alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan',
				img: require('../../Assets/doc3.jpg'),
				link: function go(nama, alamat, img) { Actions.bidanAccount({ nama, alamat, img }); }
			},
			{
				nama: 'M. Satrya D.S',
				alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan',
				img: require('../../Assets/doc4.jpg'),
				link: function go(nama, alamat, img) { Actions.bidanAccount({ nama, alamat, img }); }
			},
			{
				nama: 'Aprilia Yusrina',
				alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan',
				img: require('../../Assets/doc5.jpg'),
				link: function go(nama, alamat, img) { Actions.bidanAccount({ nama, alamat, img }); }
			},
			{
				nama: 'M. Satrya D.S',
				alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan',
				img: require('../../Assets/doc6.jpeg'),
				link: function go(nama, alamat, img) { Actions.bidanAccount({ nama, alamat, img }); }
			},

		]
	}
	render() {
		const {
			container,
			header,
			infoContainer,
			cardContainer,
			contentContainer,
			inputContainer
		} = styles;

		return (
			<View style={container}>
				<View style={header}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
              <Icon 
                name="angle-left"
                size={30}
                color="#ffffff"
              />
            </TouchableOpacity>
						<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Bidan</Text>
					</View>

					<View 
						style={inputContainer}
					>
						<Icon 
              name="search"
              size={20}
              style={{ marginRight: 25 }}
              color="#ffffff"
            />
						<TextInput
							placeholder='Cari Berdasarkan nama Bidan'
							placeholderTextColor='#ffffff'
							style={{ color: '#ffffff' }}
							underlineColorAndroid="transparent"
						/>
					</View>
				</View>

				<View style={contentContainer}>

					<FlatList
						data={this.state.bidan}
						numColumns={2}
						showsVerticalScrollIndicator={false}
						columnWrapperStyle={{ justifyContent: 'space-around', paddingTop: 20 }}
						keyExtractor={(item, index) => index.toString()}
						renderItem={({ item, index }) =>
							<TouchableOpacity style={cardContainer} onPress={() => item.link(item.nama, item.alamat, item.img)}>
								<View style={{ elevation: 6, position: 'absolute', top: 0, }}>
										<Image 
											style={{ width: 80, height: 80, borderRadius: 40 }}
											source={item.img}
										/>
								</View>

								<View style={infoContainer}>
									<View style={{ flex: 1, justifyContent: 'space-around', alignItems: 'center' }}>
										<Text style={{ fontSize: 14, fontWeight: '900', color: '#231f20' }}>{item.nama}</Text>
										<Text style={{ fontSize: 12, color: '#231f20' }}>{item.alamat}</Text>
									</View>
								</View>
							</TouchableOpacity>
						}
					/>
				</View>

						
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		//justifyContent: 'center',
	},
	header: {
		height: 180,
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE',
		justifyContent: 'space-between',
		paddingTop: 40,
		paddingBottom: 30
	},
	infoContainer: { 
		alignItems: 'center', 
		height: 210, 
		width: '100%', 
		elevation: 5, 
		backgroundColor: '#ffffff',
		position: 'absolute',
		bottom: 0,
		paddingHorizontal: 20,
		paddingTop: 45,
		paddingBottom: 15,
		borderRadius: 5
	},
	cardContainer: { 
		width: 150,
		height: 250,
		marginBottom: 20,
		alignItems: 'center',
	},
	contentContainer: {
		paddingHorizontal: 10,
		flex: 1,
	},
	inputContainer: { 
		alignItems: 'center', 
		height: 50, 
		flexDirection: 'row', 
		width: '100%', 
		backgroundColor: '#ABD2FC', 
		borderRadius: 5, 
		paddingHorizontal: 15 
	}

});
