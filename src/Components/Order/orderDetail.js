import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Modal } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class InfoDetail extends Component {
	state = {
		confirmYes: false,
		confirmBatal: false,
	}

	render() {
		const {
			header,
			card,
			header2,
			line,
			totalTextt,
			orderButton,
			orderButton2,
			buttonContainer,
			modalButton,
			modalCont,
			modalButtonCont,
			modalCard,
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>
						Order Detail{'\n'}
						<Text style={{ fontSize: 16, fontWeight: '900', color: '#ffffff' }}>
							(#ID: 3242349872)
						</Text>
					</Text>
				</View>
				<View style={header2} />
				
				<View style={{ paddingHorizontal: 20 }}>
					<View style={card}>
						<Text style={{ fontSize: 16, fontWeight: '600', color: '#231f20', marginBottom: 10 }}>Order Detail</Text>
						
						<View style={{ flexDirection: 'row', marginBottom: 10 }}>
							<Image
								style={{ width: 80, height: 80, marginRight: 10, borderRadius: 5 }}
								source={this.props.img}
							/>
							<View>
								<Text style={{ fontSize: 10, color: '#231f20' }}>{this.props.names}</Text>
								<Text style={{ fontSize: 16, fontWeight: '600', color: '#231f20' }}>{this.props.price}</Text>
							</View>
						</View>
						
						<View style={line} />

						<Text style={{ fontSize: 10, fontWeight: '600', color: '#231f20' }}>Lokasi Kirim</Text>
						<Text style={{ fontSize: 10, marginBottom: 10 }}>{this.props.alamat}</Text>
						
						<View style={line} />

						<Text style={{ fontSize: 10, fontWeight: '600', color: '#231f20' }}>Detail Pembayaran</Text>
						<View style={totalTextt}>
							<Text style={{ fontSize: 10 }}>Total Pembayaran</Text>
							<Text style={{ fontSize: 16, fontWeight: '600', color: '#7AB4FE' }}>{this.props.price}</Text>
						</View>
					</View>
					
				</View>
				<View style={buttonContainer}>
					<TouchableOpacity style={orderButton} onPress={() => this.setState({ confirmBatal: true })}>
						<Text style={{ fontSize: 14, color: '#ffffff', fontWeight: '500' }}>BATAL</Text>
					</TouchableOpacity>

					<TouchableOpacity style={orderButton2} onPress={() => this.setState({ confirmYes: true })}>
						<Text style={{ fontSize: 14, color: '#ffffff', fontWeight: '500' }}>BAYAR</Text>
					</TouchableOpacity>
				</View>

				<Modal
					animationType="slide"
					visible={this.state.confirmBatal}
					transparent
					onRequestClose={() => {}}
				>
					<View style={modalCont}>
						<View style={modalCard}>
							<Text style={{ fontSize: 16, fontWeight: '900', color: '#231f20', marginBottom: 20, }}>Konfirmasi Pembatalan</Text>
							<Text style={{ fontSize: 14, fontWeight: '600', marginBottom: 40, textAlign: 'center' }}>Apakah anda yakin ingin membatalkan pesanan ini</Text>

							<View style={modalButtonCont}>
								<TouchableOpacity onPress={() => this.setState({ confirmBatal: false })} style={modalButton}>
									<Text style={{ fontSize: 10, fontWeight: '500' }}>TIDAK</Text>
								</TouchableOpacity>

								<TouchableOpacity onPress={() => this.setState({ confirmBatal: false })} style={{ ...modalButton, backgroundColor: '#F88C8F' }}>
									<Text style={{ fontSize: 10, color: '#ffffff', fontWeight: '500' }}>BATALKAN</Text>
								</TouchableOpacity>
							</View>
						</View>
					</View>
				</Modal>

				<Modal
					animationType="slide"
					visible={this.state.confirmYes}
					transparent
					onRequestClose={() => {}}
				>
					<View style={modalCont}>
						<View style={modalCard}>
							<Text style={{ fontSize: 16, fontWeight: '900', color: '#231f20', marginBottom: 20, }}>Berhasil</Text>
							<Text style={{ fontSize: 14, fontWeight: '600', marginBottom: 40, textAlign: 'center' }}>Silahkan transfer ke Rekening Senusa, dan segera konfirmasi</Text>

	
								<TouchableOpacity onPress={() => this.setState({ confirmYes: false })} style={{
									...modalButton,
									width: '100%',
									alignItems: 'flex-end',
								}}>
									<Text style={{ fontSize: 10, fontWeight: '500' }}>OK</Text>
								</TouchableOpacity>
						</View>
					</View>
				</Modal>

			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 16,
		fontWeight: '900',
		color: '#231f20'
	},
	textt2: {
		fontSize: 16,
		color: '#231f20'
	},
	header: {
		height: 80,
		flexDirection: 'row',
		paddingTop: 20,
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	card: { 
		padding: 15,
		width: '100%',
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5,
	},
	header2: {
		height: 60,
		backgroundColor: '#7AB4FE',
		position: 'absolute',
		top: 80,
		left: 0,
		right: 0,
	},
	line: { 
		borderTopWidth: 1, 
		borderColor: '#D3D3D3', 
		width: '100%', 
		marginBottom: 10 
	},
	totalTextt: { 
		flexDirection: 'row', 
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	orderButton: { 
		alignItems: 'center',
		width: '45%', 
		justifyContent: 'center', 
		backgroundColor: '#808080', 
		paddingHorizontal: 30,
		paddingVertical: 10,
		borderRadius: 5,
		marginBottom: 15
	},
	orderButton2: { 
		alignItems: 'center',
		width: '45%', 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		paddingHorizontal: 30,
		paddingVertical: 10,
		borderRadius: 5,
		marginBottom: 15
	},
	buttonContainer: { 
		flexDirection: 'row', 
		justifyContent: 'space-between', 
		position: 'absolute', 
		bottom: 0,
		left: 20,
		right: 20,
	},
	modalCont: {
		backgroundColor: 'rgba(0,0,0,0.5)',
		paddingHorizontal: 55,
		alignItems: 'center',
		justifyContent: 'center',
		flex: 1
	},
	modalCard: {
		width: '100%',
		backgroundColor: '#ffffff',
		paddingHorizontal: 20,
		paddingTop: 50,
		paddingBottom: 30,
		alignItems: 'center',
		borderRadius: 5,
	},
	modalButtonCont: { 
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center', 
	},
	modalButton: {
		alignItems: 'center', 
		justifyContent: 'center', 
		paddingHorizontal: 20,
		paddingVertical: 10,
		borderRadius: 5,
	}
};
