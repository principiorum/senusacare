import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicon from 'react-native-vector-icons/Ionicons';
import ModalDropdown from 'react-native-modal-dropdown';

const width = Dimensions.get('window').width;

export default class KonfirmasiPesananBidan extends Component {
	state = {
		bankSend: ['BCA', 'BNI', 'BJB', 'BTPN', 'Mandiri', 'BRI'],
		bankSendChoosen: null,
		bankTo: ['BCA', 'BNI', 'BJB', 'BTPN', 'Mandiri', 'BRI'],
		bankToChoosen: null,
		method: ['M-Banking', 'Transfer'],
		methodChoosen: null,
	}

	render() {
		const {
			header,
			header2,
			textt,
			texttt,
			textt2,
			card,
			iconCont,
			orderButton,
			promoCode,
			line,
			textt3,
			dropDownContainer
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
						<Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
						/>
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Konfirmasi Pesanan</Text>
				</View>
				<View style={header2} />
				<ScrollView>
				<View style={{ flex: 1 }}>
					{
						//Top card Lokasi dan waktu pemesanan
					}
					<View style={card}>
						<Text style={textt2}>Total Pembayaran</Text>
						<Text style={textt}>Rp 60.022</Text>
						<View style={iconCont}>
							<Ionicon
								name="ios-arrow-round-up"
								size={60}
								color="#231f20"
							/>
						</View>

						<Text style={texttt}>
							Upload hasil foto bukti transfer Anda atau screenshot bukti dari e-banking.
							Senusa akan memeriksa bukti Anda dalam 24 jam
						</Text>

						<View style={line} />

						<Text style={textt3}>Nama Pengirim</Text>
						<TextInput
							style={promoCode}
							placeholder='Nama Lengkap'
							placeholderTextColor='#231f20'
						/>

						<Text style={textt3}>Transfer dari Bank</Text>
						<View style={dropDownContainer}>
							<ModalDropdown 
								options={this.state.bankSend}
								style={{ flex: 1 }}
								dropdownStyle={{ width: width - 70, height: 160 }}
								dropdownTextStyle={{ color: '#231f20', fontSize: 12 }}
								textStyle={{ color: '#231f20', fontSize: 12 }}
								animated
								defaultValue='Pilih Bank'
								onSelect={(values) => this.setState({ bankSendChoosen: values })}
							/>
							<Icon 
								name="caret-down"
								size={18}
								color="#231f20"
							/>
						</View>

						<Text style={textt3}>Transfer ke Bank</Text>
						<View style={dropDownContainer}>
							<ModalDropdown 
								options={this.state.bankTo}
								style={{ flex: 1 }}
								dropdownStyle={{ width: width - 70, height: 160 }}
								dropdownTextStyle={{ color: '#231f20', fontSize: 12 }}
								textStyle={{ color: '#231f20', fontSize: 12 }}
								animated
								defaultValue='Pilih Bank'
								onSelect={(values) => this.setState({ bankToChoosen: values })}
							/>
							<Icon 
								name="caret-down"
								size={18}
								color="#231f20"
							/>
						</View>

						<Text style={textt3}>Metode Transfer</Text>
						<View style={dropDownContainer}>
							<ModalDropdown 
								options={this.state.method}
								style={{ flex: 1 }}
								dropdownStyle={{ width: width - 70, height: 80 }}
								dropdownTextStyle={{ color: '#231f20', fontSize: 12 }}
								textStyle={{ color: '#231f20', fontSize: 12 }}
								animated
								defaultValue='Pilih Metode'
								onSelect={(values) => this.setState({ methodChoosen: values })}
							/>
							<Icon 
								name="caret-down"
								size={18}
								color="#231f20"
							/>
						</View>

					</View>
				</View>

				<TouchableOpacity style={orderButton}>
					<Text style={{ fontSize: 14, color: '#ffffff', fontWeight: '500' }}>KONFIRMASI PEMBAYARAN</Text>
				</TouchableOpacity>
				</ScrollView>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: { 
		fontSize: 20, 
		color: '#7AB4FE', 
		fontWeight: '900',
		marginBottom: 15
	},
	texttt: { 
		fontSize: 12,
		fontWeight: '500'
	},
	textt3: { 
		fontSize: 10,
		color: '#231f20',
		fontWeight: '500'
	},
	textt2: { 
		fontSize: 14, 
		fontWeight: '900', 
		color: '#231f20',
		marginBottom: 5 
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	header2: {
		height: 60,
		backgroundColor: '#7AB4FE',
		position: 'absolute',
		top: 80,
		left: 0,
		right: 0,
	},
	card: { 
		padding: 15,
		width: width - 40,
		alignSelf: 'center',
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5,
		marginBottom: 50,
	},
	orderButton: { 
		alignItems: 'center',
		width: width - 40, 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		paddingHorizontal: 30,
		paddingVertical: 10,
		borderRadius: 5,
		alignSelf: 'center',
		marginBottom: 15
	},
	promoCode: {
		flex: 1,
		height: 40,
		backgroundColor: '#e9e9e9',
		color: '#231f20',
		borderRadius: 5,
		paddingHorizontal: 10,
		fontSize: 12,
		marginBottom: 10,
		marginTop: 5
	},
	iconCont: { 
		width: 80, 
		height: 80, 
		borderRadius: 40, 
		alignSelf: 'center', 
		justifyContent: 'center', 
		alignItems: 'center', 
		backgroundColor: '#C0C0C0',
		marginBottom: 15
	},
	line: { 
		borderTopWidth: 1, 
		borderColor: '#D3D3D3', 
		width: '100%', 
		marginBottom: 15,
		marginTop: 15 
	},
	dropDownContainer: { 
		alignItems: 'center', 
		flexDirection: 'row', 
		width: '100%',
		height: 40,
		borderRadius: 5,
		paddingHorizontal: 10,
		paddingVertical: 12,
		backgroundColor: '#e9e9e9',
		marginTop: 5,
		marginBottom: 10
	},
};
