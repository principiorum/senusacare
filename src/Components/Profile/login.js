import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TextInput, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// #A2CAFF background logo
// #F88C8F button 

const width = Dimensions.get('window').width;

export default class Login extends Component {

	render() {
		const {
			header,
			inputStyle,
			button,
			button2,
			logo
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Login</Text>
				</View>
				
				<KeyboardAwareScrollView>
				<Image 
					style={logo}
					source={require('../../Assets/logofix.png')}
				/>

				<View style={{ paddingHorizontal: 20 }}>
					<TextInput 
						style={inputStyle}
						placeholder='Email'
						placeholderTextColor='#2945a7'
						underlineColorAndroid='transparent'
					/>

					<TextInput 
						style={inputStyle}
						placeholder='Password'
						secureTextEntry
						placeholderTextColor='#2945a7'
						underlineColorAndroid='transparent'
					/>

					<TouchableOpacity style={button}>
						<Text style={{ fontSize: 14, fontWeight: '800', color: '#ffffff' }}>LOGIN</Text>
					</TouchableOpacity>

					<View style={{ justifyContent: 'flex-end', flexDirection: 'row', width: '100%', marginBottom: 30 }}>
						<Text style={{ fontSize: 12, color: '#2945a7' }}>Belum punya akun?&nbsp;</Text>
						<TouchableOpacity onPress={() => Actions.registrasi()}>
							<Text style={{ fontSize: 12, fontWeight: '800', color: '#2945a7' }}>Daftar</Text>
						</TouchableOpacity>
					</View>

					<Text style={{ fontSize: 14, fontWeight: '900', color: '#ffffff', alignSelf: 'center', marginBottom: 15 }}>atau</Text>
					
					<View style={{ justifyContent: 'space-between', flexDirection: 'row', width: '100%' }}>
						<TouchableOpacity style={button2}>
							<Image
								style={{ width: 35, height: 35, marginRight: 5 }}
								source={require('../../Assets/facebook2.png')}
							/>
							<View>
								<Text style={{ fontSize: 10, color: '#231f20' }}>login with</Text>
								<Text style={{ fontSize: 14, fontWeight: '500', color: '#231f20' }}>FACEBOOK</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity style={button2}>
							<Image
								style={{ width: 35, height: 35, marginRight: 5 }}
								source={require('../../Assets/google.png')}
							/>
							<View>
								<Text style={{ fontSize: 10, color: '#231f20' }}>login with</Text>
								<Text style={{ fontSize: 14, fontWeight: '500', color: '#231f20' }}>GOOGLE</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
				</KeyboardAwareScrollView>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
		backgroundColor: '#7AB4FE'
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	inputStyle: {
		width: '100%',
		height: 40,
		textAlign: 'center',
		marginBottom: 10,
		color: '#2945a7',
		backgroundColor: '#A2CAFF',
		borderRadius: 5
	},
	button: {
		width: '100%',
		height: 40,
		alignItems: 'center',
		justifyContent: 'center',
		marginBottom: 10,
		backgroundColor: '#F88C8F',
		borderRadius: 5
	},

	button2: {
		width: '47%',
		height: 45,
		alignItems: 'center',
		justifyContent: 'center',
		marginBottom: 10,
		backgroundColor: '#ffffff',
		borderRadius: 5,
		flexDirection: 'row',
		paddingHorizontal: 15
	},
	logo: { 
		width: 160, 
		height: 160, 
		marginTop: 10, 
		marginBottom: 40, 
		backgroundColor: 
		'#A2CAFF', 
		borderRadius: 80,
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems: 'center'
	}
};
