/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, Text, Dimensions, View, ScrollView, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const width = Dimensions.get('window').width;

export default class Profile extends Component {
  render() {
    const {
      container,
      header,
      absoluteComp,
      pictContainer,
      cardBot,
      cardTop,
      dot
    } = styles;

    return (
      <View style={container}>
        <View style={header}>
          <Text style={{ fontSize: 16, fontWeight: '900', color: '#ffffff' }}>Profil</Text>
        </View>

        <ScrollView>

          <View style={absoluteComp}>
            <View style={pictContainer}>
              <Image
                style={{ width: 114, height: 114, borderRadius: 57 }}
                source={require('../../Assets/doc1.jpg')}
              />
            </View>
            <Text style={{ fontSize: 16, marginTop: 5, fontWeight: '900', color: '#ffffff', alignSelf: 'center' }}>M. Shoufi</Text>
          </View>

          <View style={cardTop}>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontSize: 16, fontWeight: '900', color: '#231f20' }}>AKUN</Text>
              <TouchableOpacity style={{ position: 'absolute', right: 5 }}>
                <Text style={{ fontSize: 12, fontWeight: '900', color: '#231f20' }}>Edit</Text>
              </TouchableOpacity>
            </View>

            <View>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View style={dot} />
                <Text style={{ fontSize: 12, fontWeight: '900', color: '#231f20' }}>Alamat</Text>
              </View>
              <Text style={{ marginLeft: 15, fontSize: 12, color: '#231f20' }}>Jl. Merauke No 36, Jakarta Selatan</Text>
            </View>

            <View>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View style={dot} />
                <Text style={{ fontSize: 12, fontWeight: '900', color: '#231f20' }}>No Telepon</Text>
              </View>
              <Text style={{ marginLeft: 15, fontSize: 12, color: '#231f20' }}>085215055555</Text>
            </View>

            <View>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View style={dot} />
                <Text style={{ fontSize: 12, fontWeight: '900', color: '#231f20' }}>Email</Text>
              </View>
              <Text style={{ marginLeft: 15, fontSize: 12, color: '#231f20' }}>wildan@gmail.com</Text>
            </View>

          </View>

          <View style={cardBot}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontSize: 16, fontWeight: '900', color: '#231f20' }}>TENTANG</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View style={dot} />
              <Text style={{ fontSize: 12, fontWeight: '900', color: '#231f20' }}>Tentang Senusa Care</Text>
              <TouchableOpacity style={{ position: 'absolute', right: 5 }}>
                <Icon 
                  name="angle-down"
                  size={20}
                  color="#9b9b9b"
                />
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View style={dot} />
              <Text style={{ fontSize: 12, fontWeight: '900', color: '#231f20' }}>Ketentuan Layanan</Text>
              <TouchableOpacity style={{ position: 'absolute', right: 5 }}>
                <Icon 
                  name="angle-down"
                  size={20}
                  color="#9b9b9b"
                />
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View style={dot} />
              <Text style={{ fontSize: 12, fontWeight: '900', color: '#231f20' }}>Social Media</Text>
            </View>

            <View style={{ marginLeft: 15, flexDirection: 'row', alignItems: 'center' }}>
              <Image
                style={{ width: 15, height: 15, marginRight: 5 }}
                source={require('../../Assets/facebook.png')}
              />
              <Text style={{ fontSize: 12, fontWeight: '900', color: '#231f20' }}>Senusa Care</Text>
            </View>

            <View style={{ marginLeft: 15, flexDirection: 'row', alignItems: 'center' }}>
              <Image
                style={{ width: 15, height: 15, marginRight: 5 }}
                source={require('../../Assets/instagram.png')}
              />
              <Text style={{ fontSize: 12, fontWeight: '900', color: '#231f20' }}>@senusacare</Text>
            </View>

          </View>


        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
  },
  header: {
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#7AB4FE',
  },
  cardBot: { 
    width: width - 40,
    height: 200,
    marginTop: 30,
    backgroundColor: '#ffffff',
    alignSelf: 'center',
    elevation: 5,
    borderRadius: 5,
    marginBottom: 20,
    padding: 10,
    justifyContent: 'space-between'
  },
  cardTop: { 
    width: width - 40,
    height: 200,
    marginTop: 170,
    backgroundColor: '#ffffff',
    alignSelf: 'center',
    elevation: 5,
    borderRadius: 5,
    padding: 10,
    justifyContent: 'space-between'
  },
  absoluteComp: { 
    width: '100%',
    height: 220,
    backgroundColor: '#7AB4FE',
    paddingHorizontal: 20,
    position: 'absolute',
    top: 0
  },
  pictContainer: { 
    width: 120,
    height: 120,
    backgroundColor: '#ffffff',
    borderRadius: 60,
    alignSelf: 'center',
    marginTop: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  dot: { 
    width: 10, 
    marginRight: 5, 
    height: 10, 
    borderRadius: 5, 
    backgroundColor: '#FF0040' 
  }

});

export { Profile };
